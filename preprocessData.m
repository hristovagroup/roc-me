function data = preprocessData(data,targetSize)
% Resize image
scale = targetSize(1:2)./size(data,[1 2]);
data = imresize(data,targetSize(1:2));
if targetSize(3) == 3
   data = cat(3, data, data, data); 
end
data = double(data);
end