% ROC-ME (Recognition of cellular membranes) software

% This code finds regions of diffraction limited membrane in fluorescent
% microscope images and creates a semantic segmentation map with associated
% scores. It is based on two neural networks in series - an object
% detection network followed by a semantic segmentation network. 
% Multiple neural networks have been trained and their files are saved in the
% trained neural networks folder. By replacing the filenames in lines 17
% and 18 different neural networks can be chosen. 
% The user is asked to select a folder containing the microscope scans and
% the ROC-ME software will create a cell array with the following
% information as an output:
% - filename of the analyzed image
% - detected diffraction-limited ROI in image
% - score of the diffraction-limited ROI
% - segmentation map of the diffraction limited ROI
% - scores for the segmentation map
% - scores of every class for every pixel of the segmentation map
% (allscores)

% The ROC-ME software is open-source under the Apache License 2.0. 
% © 2020 The Johns Hopkins University 
% Please cite the authors when using this software.
% Last modified: 2/8/2021 Daniel Wirth

function [imagedata] = rocme

% here the user specifies the folder location containing the microscope
% images
path = uigetdir('C:\','Select the folder containing the images...');
imds = imageDatastore(path);

% load the object detecrion (RCNN) and semantic segmentation (seg) network.
% Change the network filename to use a different network.
rcnnnetwork = load(strcat(pwd,'\trained neural networks\resnet18.mat'),'detector');
segnetwork = load(strcat(pwd,'\trained neural networks\deeplabv3plus_resnet50_nobal_noaug_detector.mat'),'net');



% this is the image size the network expects as an input
inputSize = [224 224 3]; 

% ... so the loaded microscope images need to be transformed
imds= transform(imds,@(data)preprocessData(data,inputSize));

% run the object detection network on the transformed image datastore.
% detectionResults will include the locations of objects detected with
% associated labels and scores. The paper renamed the classes homogenous
% membrane, heterogenous membrane, bad membrane,and bad cells 
% to diffraction-limited membrane, imperfect membrane and out-of-focus
% cells for clarity. diffraction-limited membrane = homogenous membrane;
% imperfect membrane = bad membrane and heterogenous membrane; out-of-focus
% cells = bad cells.

detectionResults = detect(rcnnnetwork.detector,imds);


%extract only the homogenous membrane bboxes, scores, labels and put them in vesicle
%Result table
r=height(detectionResults);
cellResults = table('Size',[r 4],'VariableTypes',{'cell','cell','cell','cell'},...
    'VariableNames',{'imageFilename','Boxes','Scores','Labels'});
for i = 1:r
    labels_temp = detectionResults.Labels{i,1};
    scores_temp = detectionResults.Scores{i,1};
    cell_idx = (labels_temp=='homogenous_membrane');
    labels_temp = labels_temp(cell_idx==1);
    scores_temp = scores_temp(cell_idx==1);
    boxes_cell = detectionResults.Boxes{i,1}(cell_idx==1,:);
    cellResults.Scores{i,1} = scores_temp;
    cellResults.Boxes{i,1} = boxes_cell;
    cellResults.Labels{i,1} = labels_temp;
    cellResults.imageFilename{i,1} = imds.UnderlyingDatastore.Files{i};
end

% create new table with homogenous membrane ROIs with a score higher than
% 0.5 and sort them by descending scores
cellResults2 = table('Size',[r 4],'VariableTypes',{'cell','cell','cell','cell'},...
    'VariableNames',{'imageFilename','Boxes','Scores','Labels'});
for i = 1:height(cellResults)
    indices = find(cellResults.Scores{i}>0.5);
    for k = 1:length(indices)
        if i == 1
        index_box = indices(k);
    cellResults2.Scores{k} = cellResults.Scores{i}(index_box);
    cellResults2.Boxes{k} = cellResults.Boxes{i}(index_box,:);
    cellResults2.Labels{k} = cellResults.Labels{i}(index_box);
    cellResults2.imageFilename{k} = cellResults.imageFilename{i};
        else 
             index_box = indices(k);
             idx_table = k+height(cellResults2);
          cellResults2.Scores{idx_table} = cellResults.Scores{i}(index_box);
    cellResults2.Boxes{idx_table} = cellResults.Boxes{i}(index_box,:);
    cellResults2.Labels{idx_table} = cellResults.Labels{i}(index_box);
    cellResults2.imageFilename{idx_table} = cellResults.imageFilename{i}; 
        end
    end
end
idx_cellResults2=all(cellfun(@isempty,cellResults2{:,:}),2);
cellResults2(idx_cellResults2,:)=[];
cellResults2_sorted = sortrows(cellResults2,3,'descend');

% rescale the selected ROIs 
  scale = [300 440]./[224 224];
    for i = 1:height(cellResults2_sorted)
    cellResults2_sorted.Boxes{i} = bboxresize(cellResults2_sorted.Boxes{i},scale);
    end
    
   
% run the semantic segmentation network for each identified homogenous
% membrane ROI
for boxcount = 1:height(cellResults2_sorted)
ROI = cellResults2_sorted.Boxes{boxcount};
img_sel = imcrop(imread(cellResults2_sorted.imageFilename{boxcount}),ROI);

% transform selected image in  expected input format for semantic
% segmentation network 
img_sel_transformed = cat(3, img_sel,img_sel,img_sel);
img_sel_transformed = double(imresize(img_sel_transformed,[224 224]));


% run the semantic segmentation network. Returns the segmentation map seg,
% the scores for the segmentation map seg and the scores for all labels at
% every pixel allscores.
[seg, scores, allscores] = semanticseg(img_sel_transformed,segnetwork.net);


% resize the segmentation map and scores to the original image
seg = imresize(seg,[size(img_sel,1) size(img_sel,2)]);
scores = imresize(scores,[size(img_sel,1) size(img_sel,2)]);
allscores = imresize(allscores,[size(img_sel,1) size(img_sel,2)]);


% export the results of the object detection and semantic segmentation
% network to the cell array data
check = exist('imagedata');
     if check == 0
        imagedata = [{'imageFilename'},{'Boxes'},{'Scores'},{'Labels'},{'segmentation'},{'seg_score'},{'seg_allscores'}]; 
     end
     datapoint = [{cellResults2_sorted.imageFilename{boxcount}}, {cellResults2_sorted.Boxes{boxcount}},  {cellResults2_sorted.Scores{boxcount}}, {cellResults2_sorted.Labels{boxcount}}, {seg}, {scores}, {allscores}];
     imagedata = [imagedata; datapoint];  
     
     end

end