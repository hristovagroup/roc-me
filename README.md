# ROC-ME
ROC-ME (Recognition Of Cellular Membranes) is software to identify stretches of diffraction-limited membrane in fluorescent microscopy images. It is based on a series of neural networks - an object detection and a semantic segmentation network.

## Requirements
Matlab toolboxes needed:
1. Deep learning toolbox
2. Computer vision toolbox
3. Image processing toolbox


## How to use
Run the following line in Matlab's command window
```
[imagedata] = rocme
```
The program will prompt you to select the folder containing your microsope images. 
The cell array imagedata will appear in Matlab's workspace with:
- the filename of the analyzed image
- the location of the diffraction-limited membrane ROI
- the segmentation map of the diffraction-limited membrane ROI
- scores for the segmentation map
- scores of every class for every pixel of the segmentation map

Example images are provided in the `\images` folder.

## Trained neural networks
All trained neural networks can be found in the folder `\trained neural networks`.

You can choose to use a different neural network by changing the filename in lines 35 (object detector) and 36 (semantic segmentation network).
For example if you want to change the semantic segmentation network Resnet50 to InceptionResnetv2 (both nobal, noaug), then change line 36 from
```
segnetwork = load(strcat(pwd,'\trained neural networks\deeplabv3plus_resnet50_nobal_noaug_detector.mat'),'net');
```
to 
```
segnetwork = load(strcat(pwd,'\trained neural networks\inceptionresnetv2_nobal_noaug.mat'),'net');
```
assuming the trained InceptionResnetv2 is located in the folder `\trained neural networks`.

## License
The software is open source under the Apache-2.0 License. Feel free to use the software, but please cite its authors. 
© 2020 The Johns Hopkins University, C16478 
